package tinkersolutions.com.dperfumary.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tinkersolutions.com.dperfumary.Items.ItemNamePrice;
import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.SharedUserData;

public class HomePageAdapter extends BaseAdapter {


    ImageView imageViewItem, imageViewCart;
    TextView textViewName, textViewPrice;
    SharedUserData sharedUserData;



    private Context mContext;
    private ItemNamePrice itemNamePrice;
    private ArrayList<String> perfumeName=new ArrayList<>();

    private int size,minimum_order;


    public HomePageAdapter(Context c, ItemNamePrice itemNamePrice, int size) {

        mContext=c;
        this.itemNamePrice = itemNamePrice;
        this.size=size;
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sharedUserData=new SharedUserData(mContext);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.list_item, null);


            imageViewItem =(ImageView)grid.findViewById(R.id.image1);
            Picasso.with(mContext)
                    .load(itemNamePrice.getPerfumeURL().get(position))
                    .placeholder(R.drawable.per) // optional
                    .error(R.drawable.per)       // optional
                    .into(imageViewItem);




            textViewName =grid.findViewById(R.id.item_name);
            textViewName.setText(itemNamePrice.getPerfumeName().get(position));

            textViewPrice =grid.findViewById(R.id.item_price);
            textViewPrice.setText("Rs. "+String.valueOf(itemNamePrice.getPerfumePrice().get(position)));

            imageViewCart =grid.findViewById(R.id.ic_cart);

            imageViewCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(itemNamePrice.getPerfumeAvailableQuantity().get(position)>=itemNamePrice.getPerfumeMinimumOrder().get(position))
                    {
                        sharedUserData.addItem(itemNamePrice.getPerfumeId().get(position),itemNamePrice.getPerfumeMinimumOrder().get(position));

                        Toast.makeText(mContext, "Item added to cart. ", Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(mContext, "Item out of stock. Please come after some time", Toast.LENGTH_SHORT).show();

                }
            });

           // ImageView imageViewItem=(ImageView)grid.findViewById(R.id.image1);
            //imageViewItem.setImageResource(Imageid[position]);
        }
        else{
            grid = (View) convertView;
        }
        return grid;
    }
}
