package tinkersolutions.com.dperfumary.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import tinkersolutions.com.dperfumary.JsonParser.Perfumes;
import tinkersolutions.com.dperfumary.Options.CartListActivity;
import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.ItemIdQuantity;
import tinkersolutions.com.dperfumary.SharedPreference.SharedLoginData;
import tinkersolutions.com.dperfumary.SharedPreference.SharedUserData;

public class CartAdapter extends BaseAdapter {

    Context context;
    List<Perfumes> orderList = new ArrayList<>();
    List<ItemIdQuantity> itemIdQuantityList=new ArrayList<>();

    ImageView imageView;
    TextView itemName, itemPrice, itemQuantity, totalPrice;
    LinearLayout editLayout, removeLayout;

    SharedUserData sharedUserData;
    SharedLoginData sharedLoginData;

    CartListActivity cartListActivity;
    public CartAdapter(Context context, List<Perfumes> orderList,List<ItemIdQuantity> itemIdQuantityList) {

        this.context = context;
        this.orderList = orderList;
        this.itemIdQuantityList=itemIdQuantityList;

    }


    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View grid;


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {

            grid = inflater.inflate(R.layout.layout_cartlist_item, null);

            sharedUserData = new SharedUserData(context);
            sharedLoginData=new SharedLoginData(context);

            cartListActivity=new CartListActivity();

            imageView = grid.findViewById(R.id.image_cartlist);
            itemName = grid.findViewById(R.id.item_name);
            itemPrice = grid.findViewById(R.id.item_price);
            itemQuantity = grid.findViewById(R.id.item_quantity);
            totalPrice = grid.findViewById(R.id.item_totalprice);
            editLayout = grid.findViewById(R.id.layout_edit);
            removeLayout = grid.findViewById(R.id.layout_remove);


            //item_id = orderList.get(position).getPerfumeId();
            //put the condition here aling with dialog box
            //item_minimumquantity = orderList.get(position).getMinimumOrder().getDealerMinimumOrder();
            //item_quantity=sharedUserData.getQuantity(orderList.get(position).getPerfumeId());
            //put the if condition here ie. for customer dealer etc.
            //total_price = item_price * sharedUserData.getQuantity(orderList.get(position).getPerfumeId());


            final int item_price = orderList.get(position).getPrice().getCustomerPrice();

            Picasso.with(context)
                    .load(orderList.get(position).getImageURL())
                    .placeholder(R.drawable.per) // optional
                    .error(R.drawable.per)       // optional
                    .into(imageView);

            itemName.setText(orderList.get(position).getPerfumeName());
            itemPrice.setText("Rs.: " + item_price);
            itemQuantity.setText("Quantity :" + sharedUserData.getQuantity(orderList.get(position).getPerfumeId()));
            totalPrice.setText("Total Price :" + item_price * sharedUserData.getQuantity(orderList.get(position).getPerfumeId()));


           // cartListActivity.updateFinalPrice(sharedUserData.getCartItems());
           // Toast.makeText(context, ""+orderList.get(0).getPerfumeName(), Toast.LENGTH_SHORT).show();

            editLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int minimum_order=0;
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Enter The Quantity");

                    if(sharedLoginData.getUserType().equals("Customer"))
                    {
                        minimum_order=orderList.get(position).getMinimumOrder().getCustomerMinimumOrder();
                    }
                    else if(sharedLoginData.getUserType().equals("Distributor"))
                    {
                        minimum_order=orderList.get(position).getMinimumOrder().getDistributorMinimumOrder();
                    }
                    else if(sharedLoginData.getUserType().equals("Retailer"))
                    {
                        minimum_order=orderList.get(position).getMinimumOrder().getRetailerMinimumOrder();
                    }
                    else{
                        Toast.makeText(context, "There is some error", Toast.LENGTH_SHORT).show();
                    }


                    alertDialog.setMessage("The minimum quantity you should order is " + minimum_order);

                    final EditText input = new EditText(context);
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    input.setHint("Enter Quantity");
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    alertDialog.setView(input);
                    alertDialog.setIcon(R.drawable.per);

                    alertDialog.setPositiveButton("Confirm Quantity",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (input.getText().toString().length() > 0) {


                                        int edit_quantity = (Integer.parseInt(input.getText().toString()));
                                        //Toast.makeText(context, "" + edit_quantity, Toast.LENGTH_SHORT).show();
                                        int minimum_order1 = 0;

                                        if (sharedLoginData.getUserType().equals("Customer")) {
                                            minimum_order1 = orderList.get(position).getMinimumOrder().getCustomerMinimumOrder();
                                        } else if (sharedLoginData.getUserType().equals("Distributor")) {
                                            minimum_order1 = orderList.get(position).getMinimumOrder().getDistributorMinimumOrder();
                                        } else if (sharedLoginData.getUserType().equals("Retailer")) {
                                            minimum_order1 = orderList.get(position).getMinimumOrder().getRetailerMinimumOrder();
                                        } else {
                                            Toast.makeText(context, "There is some error", Toast.LENGTH_SHORT).show();
                                        }

                                        if (edit_quantity > minimum_order1 && edit_quantity<=orderList.get(position).getPerfumeQuantity()) {

                                            itemQuantity = null;
                                            totalPrice = null;
                                            itemQuantity = grid.findViewById(R.id.item_quantity);
                                            totalPrice = grid.findViewById(R.id.item_totalprice);
                                            sharedUserData.editItem(orderList.get(position).getPerfumeId(), edit_quantity);
                                            itemQuantity.setText("Quantityiiii :" + edit_quantity);
                                            itemQuantity.postInvalidate();
                                            // total_price = item_price * edit_quantity;
                                            totalPrice.setText("Total Price :" + item_price * edit_quantity);
                                            totalPrice.postInvalidate();
                                            // cartListActivity.updateFinalPrice(sharedUserData.getCartItems());

                                        } else if(edit_quantity<minimum_order1)
                                            Toast.makeText(context, "Cant order a quantity less than the mandatory minimum order", Toast.LENGTH_SHORT).show();
                                        else if(edit_quantity>orderList.get(position).getPerfumeQuantity())
                                        {
                                            Toast.makeText(context, "This quantity is not available", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                }
                            });

                    alertDialog.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog.show();

                }
            });

            removeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    sharedUserData.removeItem(orderList.get(position).getPerfumeId());

                    /*for(int i=0;i<orderList.size();i++)
                    {
                        if(orderList.get(i).getPerfumeId()==orderList.get(position).getPerfumeId())
                        {
                            orderList.remove(i);
                            break;
                        }
                    }*/
/*
                    for(int i=0;i<itemIdQuantityList.size();i++)
                    {
                        if(itemIdQuantityList.get(i).getItemId()==item_id)
                        {
                            itemIdQuantityList.remove(i);
                            break;
                        }
                    }
                    */

                    orderList.remove(position);
                    Toast.makeText(context, "Removed" + position, Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();


                }
            });


        } else {
            grid = (View) convertView;
        }
        return grid;
    }



}

