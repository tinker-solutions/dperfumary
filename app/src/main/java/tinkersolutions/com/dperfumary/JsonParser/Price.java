package tinkersolutions.com.dperfumary.JsonParser;

public class Price {


    private int retailerPrice;
    private int customerPrice;
    private int distributorPrice;



    public int getRetailerPrice() {
        return retailerPrice;
    }

    public void setRetailerPrice(int retailerPrice) {
        this.retailerPrice = retailerPrice;
    }

    public int getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(int customerPrice) {
        this.customerPrice = customerPrice;
    }

    public int getDistributorPrice() {
        return distributorPrice;
    }

    public void setDistributorPrice(int distributorPrice) {
        this.distributorPrice = distributorPrice;
    }
}
