package tinkersolutions.com.dperfumary.JsonParser;

public class Perfumes {

    private int perfumeId;
    private String perfumeName;
    private String perfumeDescription;
    private int perfumeQuantity;
    private String imageURL;
    private Price Price;
    private MinimumOrder MinimumOrder;

    public int getPerfumeId() {
        return perfumeId;
    }

    public void setPerfumeId(int perfumeId) {
        this.perfumeId = perfumeId;
    }

    public String getPerfumeName() {
        return perfumeName;
    }

    public void setPerfumeName(String perfumeName) {
        this.perfumeName = perfumeName;
    }

    public String getPerfumeDescription() {
        return perfumeDescription;
    }

    public void setPerfumeDescription(String perfumeDescription) {
        this.perfumeDescription = perfumeDescription;
    }



    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Price getPrice() {
        return Price;
    }

    public void setPrice(Price Price) {
        this.Price = Price;
    }

    public MinimumOrder getMinimumOrder() {
        return MinimumOrder;
    }

    public void setMinimumOrder(MinimumOrder minimumOrder) {
        this.MinimumOrder = MinimumOrder;
    }

    public int getPerfumeQuantity() {
        return perfumeQuantity;
    }

    public void setPerfumeQuantity(int perfumeQuantity) {
        this.perfumeQuantity = perfumeQuantity;
    }
}
