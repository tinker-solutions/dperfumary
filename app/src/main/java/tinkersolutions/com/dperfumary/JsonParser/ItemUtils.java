package tinkersolutions.com.dperfumary.JsonParser;

import java.util.List;

public class ItemUtils {

    private List<Perfumes> Perfumes;


    public List<Perfumes> getPerfumesList() {
        return Perfumes;
    }

    public void setPerfumesList(List<Perfumes> Perfumes) {
        this.Perfumes = Perfumes;
    }
}
