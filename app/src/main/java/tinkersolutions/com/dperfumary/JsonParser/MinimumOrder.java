package tinkersolutions.com.dperfumary.JsonParser;

public class MinimumOrder {


    private int retailerMinimumOrder;
    private int customerMinimumOrder;
    private int distributorMinimumOrder;



    public int getRetailerMinimumOrder() {
        return retailerMinimumOrder;
    }

    public void setRetailerMinimumOrder(int retailerMinimumOrder) {
        this.retailerMinimumOrder = retailerMinimumOrder;
    }

    public int getCustomerMinimumOrder() {
        return customerMinimumOrder;
    }

    public void setCustomerMinimumOrder(int customerMinimumOrder) {
        this.customerMinimumOrder = customerMinimumOrder;
    }

    public int getDistributorMinimumOrder() {
        return distributorMinimumOrder;
    }

    public void setDistributorMinimumOrder(int distributorMinimumOrder) {
        this.distributorMinimumOrder = distributorMinimumOrder;
    }
}
