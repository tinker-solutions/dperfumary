package tinkersolutions.com.dperfumary.SharedPreference;

public class ItemIdQuantity {

    private int itemId,itemQuantity;

    public ItemIdQuantity(int itemId,int itemQuantity) {
        this.itemId=itemId;
        this.itemQuantity=itemQuantity;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

}
