package tinkersolutions.com.dperfumary.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedUserData {
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Gson gson;
    public Context context;
    List<ItemIdQuantity> itemIdQuantityList = new ArrayList<>();
    ItemIdQuantity itemIdQuantity;


    public SharedUserData(){}
    public SharedUserData(Context c) {
        this.context = c;
        pref = c.getSharedPreferences("CartPref", 0);
        editor = pref.edit();
        gson = new Gson();
    }

    public void addItem(int itemId, int itemQuantity) {

        int i=0;
        String json = pref.getString("item_list", "");
        if(!json.equals("")) {
            Type listType = new TypeToken<List<ItemIdQuantity>>() {
            }.getType();
            itemIdQuantityList = gson.fromJson(json, listType);
        }
        for(i=0;i<itemIdQuantityList.size();i++)
        {
            if(itemIdQuantityList.get(i).getItemId()==itemId)
                break;
        }
        if(!(i==itemIdQuantityList.size()))
            return ;
        itemIdQuantity = new ItemIdQuantity(itemId, itemQuantity);
        itemIdQuantityList.add(itemIdQuantity);
        String itemIdQuantityListString = gson.toJson(itemIdQuantityList);
        editor.clear();
        editor.putString("item_list", itemIdQuantityListString).commit();
    }

    public void removeItem(int id) {
        String json = pref.getString("item_list", "");
        if(!json.equals("")) {
            Type listType = new TypeToken<List<ItemIdQuantity>>() {
            }.getType();
            itemIdQuantityList = gson.fromJson(json, listType);
        }

        for(int i=0;i<itemIdQuantityList.size();i++)
        {
            if(itemIdQuantityList.get(i).getItemId()==id)
            {
                itemIdQuantityList.remove(i);
                break;
            }

        }

        String itemIdQuantityListString = gson.toJson(itemIdQuantityList);
        editor.putString("item_list", itemIdQuantityListString).commit();
    }

    public void editItem(int itemId,int itemQuantity){


        String json = pref.getString("item_list", "");
        if(!json.equals("")) {
            Type listType = new TypeToken<List<ItemIdQuantity>>() {
            }.getType();
            itemIdQuantityList = gson.fromJson(json, listType);
        }
        for(int i=0;i<itemIdQuantityList.size();i++)
        {
            if(itemIdQuantityList.get(i).getItemId()==itemId)
            {
                itemIdQuantityList.get(i).setItemQuantity(itemQuantity);
                break;
            }
        }
        editor.clear();
        String itemIdQuantityListString = gson.toJson(itemIdQuantityList);
        editor.putString("item_list", itemIdQuantityListString).commit();



    }

    public List<ItemIdQuantity> getCartItems() {
        List<ItemIdQuantity> itemList=new ArrayList<>();
        String json = pref.getString("item_list", "");
        Type listType = new TypeToken<List<ItemIdQuantity>>() {
        }.getType();
        itemList = gson.fromJson(json, listType);
        return itemList;
    }

    public int getQuantity(int id){
        String json = pref.getString("item_list", "");
        Type listType = new TypeToken<List<ItemIdQuantity>>() {
        }.getType();
        List<ItemIdQuantity> itemList = gson.fromJson(json, listType);
        for(int i=0;i<itemList.size();i++)
        {
            if(itemList.get(i).getItemId()==id)
                return itemList.get(i).getItemQuantity();


        }
        return 0;

    }


}
