package tinkersolutions.com.dperfumary.StartUp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import tinkersolutions.com.dperfumary.Adapters.HomePageAdapter;
import tinkersolutions.com.dperfumary.Items.ItemDescriptionActivity;
import tinkersolutions.com.dperfumary.Items.ItemNamePrice;
import tinkersolutions.com.dperfumary.JsonParser.ItemUtils;
import tinkersolutions.com.dperfumary.JsonParser.Perfumes;
import tinkersolutions.com.dperfumary.Network.JsonApi;
import tinkersolutions.com.dperfumary.Options.CartListActivity;
import tinkersolutions.com.dperfumary.Options.MyAccountActivity;
import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.SharedLoginData;

public class HomePageActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener {


    GridView gridView;
    HomePageAdapter homePageAdapter,filterAdapter;
    Toolbar toolbar;
    ItemNamePrice itemNamePrice;
    ArrayList<String> perfumeName,perfumeDescription,perfumeURL;
    ArrayList<Integer> perfumePrice,perfumeId,perfumeMinimumOrder,perfumeAvailableQuantity;
    SearchView searchView;
    ItemUtils itemUtils;
    JsonApi jsonApi;
    //final String JSONURL="http://dperfumery.tinkersolutions.co.in/apisample.php";
    final String JSONURL="http://dperfumery.tinkersolutions.co.in/api/product/read.php";
    String jsonString,type;
    Gson gson;
    List<Perfumes> perfumesList;

    int minimum_order;

    SharedLoginData sharedLoginData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        initialise();

        setHomePageAdapter();

    }

    private void initialise() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        perfumeId=new ArrayList<>();
        perfumeName=new ArrayList<>();
        perfumePrice=new ArrayList<>();
        perfumeDescription=new ArrayList<>();
        perfumeURL=new ArrayList<>();
        perfumeMinimumOrder=new ArrayList<>();
        perfumeAvailableQuantity=new ArrayList<>();


        //get json from network
        jsonApi=new JsonApi(JSONURL);
        jsonString=jsonApi.getJsonApi();

        //parse json
        perfumesList =new ArrayList<>();
        gson=new Gson();
        itemUtils = gson.fromJson(jsonString, ItemUtils.class);
        perfumesList=itemUtils.getPerfumesList();

        //put the if condition here
        sharedLoginData=new SharedLoginData(HomePageActivity.this);
        type=sharedLoginData.getUserType();
        minimum_order=50;

    }


    private void setHomePageAdapter() {

        perfumeId.clear();
        perfumeName.clear();
        perfumePrice.clear();
        perfumeDescription.clear();
        perfumeURL.clear();
        perfumeMinimumOrder.clear();
        perfumeAvailableQuantity.clear();

        itemNamePrice=null;

        for(int i=0;i<perfumesList.size();i++)
        {
            perfumeId.add(perfumesList.get(i).getPerfumeId());
            perfumeName.add(perfumesList.get(i).getPerfumeName());
            if(type.equals("Customer"))
              perfumePrice.add(perfumesList.get(i).getPrice().getCustomerPrice());
            else if(type.equals("Retailer"))
                perfumePrice.add(perfumesList.get(i).getPrice().getRetailerPrice());
            else if(type.equals("Distributor"))
                perfumePrice.add(perfumesList.get(i).getPrice().getDistributorPrice());
            else{
                Toast.makeText(this, "Some error has occured", Toast.LENGTH_SHORT).show();
                break;
            }
            perfumeDescription.add(perfumesList.get(i).getPerfumeDescription());
            perfumeURL.add(perfumesList.get(i).getImageURL());
            if(type.equals("Customer"))
                perfumeMinimumOrder.add(perfumesList.get(i).getMinimumOrder().getCustomerMinimumOrder());
            else if(type.equals("Retailer"))
                perfumeMinimumOrder.add(perfumesList.get(i).getMinimumOrder().getRetailerMinimumOrder());
            else if(type.equals("Distributor"))
                perfumeMinimumOrder.add(perfumesList.get(i).getMinimumOrder().getDistributorMinimumOrder());
            perfumeAvailableQuantity.add(perfumesList.get(i).getPerfumeQuantity());
        }




        itemNamePrice =new ItemNamePrice(perfumeId,perfumeName,perfumePrice,perfumeURL,perfumeMinimumOrder,perfumeAvailableQuantity);
        //setting the adapter
        homePageAdapter=new HomePageAdapter(HomePageActivity.this, itemNamePrice,perfumesList.size());
        gridView=(GridView)findViewById(R.id.grid);
        gridView.setAdapter(homePageAdapter);
        gridView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


        Intent intent=new Intent(this,ItemDescriptionActivity.class);
        Toast.makeText(this, ""+adapterView.getSelectedItem(), Toast.LENGTH_SHORT).show();
        intent.putExtra("perfume_id",perfumeId.get(i));
        intent.putExtra("perfume_name",perfumeName.get(i));
        intent.putExtra("perfume_picture",perfumeURL.get(i));
        intent.putExtra("perfume_price",perfumePrice.get(i));
        intent.putExtra("perfume_description",perfumeDescription.get(i));
        intent.putExtra("perfume_available_quantity",perfumeAvailableQuantity.get(i));
        intent.putExtra("perfume_minimum_order",perfumeMinimumOrder.get(i));
        intent.putExtra("maindata",jsonString);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        searchView=(SearchView)menu.findItem(R.id.action_search).getActionView();
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
       return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            Toast.makeText(HomePageActivity.this, "Clicked"+item.getClass(), Toast.LENGTH_SHORT).show();
            searchView.setOnQueryTextListener(this);

        }else if (id == R.id.action_cart) {

            Intent intent=new Intent(HomePageActivity.this, CartListActivity.class);
            intent.putExtra("maindata",jsonString);
            startActivity(intent);
        }
        else if(id == R.id.action_myaccount){
            Toast.makeText(this, "My account page", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(HomePageActivity.this, MyAccountActivity.class));

        }
        else if(id == R.id.action_signout){

            AlertDialog alertDialog = new AlertDialog.Builder(HomePageActivity.this).create();
            alertDialog.setTitle("Sign Out");
            alertDialog.setMessage("Do you really want to sign out?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            //redirect to login page
                            Intent a = new Intent(Intent.ACTION_MAIN);
                            // a.addCategory(Intent.CATEGORY_HOME);
                            // a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(a);
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

            Toast.makeText(this, "Sign out page", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        AlertDialog alertDialog = new AlertDialog.Builder(HomePageActivity.this).create();
        alertDialog.setTitle("Exit Application");
        alertDialog.setMessage("Do you really want to exit?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent a = new Intent(Intent.ACTION_MAIN);
                        // a.addCategory(Intent.CATEGORY_HOME);
                        // a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }



    @Override
    public boolean onQueryTextSubmit(String s) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String searchToken) {

        Toast.makeText(this, searchToken, Toast.LENGTH_SHORT).show();

        if(searchToken.length()>0)
        {
            homePageAdapter=null;
            perfumeId.clear();
            perfumeName.clear();
            perfumePrice.clear();
            perfumeDescription.clear();
            perfumeAvailableQuantity.clear();
            itemNamePrice=null;



            Toast.makeText(this, ""+perfumeName.size(), Toast.LENGTH_SHORT).show();



            for(int i=0;i<perfumesList.size();i++)
            {
                if(perfumesList.get(i).getPerfumeName().toLowerCase().contains(searchToken.toLowerCase()))
                {
                    perfumeId.add(perfumesList.get(i).getPerfumeId());
                    perfumeName.add(perfumesList.get(i).getPerfumeName());
                    perfumePrice.add(perfumesList.get(i).getPrice().getCustomerPrice());
                    perfumeDescription.add(perfumesList.get(i).getPerfumeDescription());
                    perfumeAvailableQuantity.add(perfumesList.get(i).getPerfumeQuantity());
                }
            }


            itemNamePrice =new ItemNamePrice(perfumeId,perfumeName,perfumePrice,perfumeURL, perfumeMinimumOrder, perfumeAvailableQuantity);
            filterAdapter=new HomePageAdapter(HomePageActivity.this, itemNamePrice,perfumeId.size());
            gridView.setAdapter(filterAdapter);
            gridView.setOnItemClickListener(this);



        }
        else
            setHomePageAdapter();


        return false;
    }




}
