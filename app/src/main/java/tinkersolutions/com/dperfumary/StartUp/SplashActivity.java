package tinkersolutions.com.dperfumary.StartUp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.SharedLoginData;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {


    Animation animFadeIn;
    RelativeLayout relativeLayout;
    SharedLoginData sharedLoginData;
    Boolean isLoggedin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }
        // load the animation
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_fade_in);
        // set animation listener
        animFadeIn.setAnimationListener(this);
        // animation for image
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        // start the animation
        relativeLayout.setVisibility(View.VISIBLE);
        relativeLayout.startAnimation(animFadeIn);



    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public void onAnimationStart(Animation animation) {

        sharedLoginData =new SharedLoginData(this);
        isLoggedin= sharedLoginData.isLoggedIn();
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        if(isLoggedin){
            Intent i = new Intent(SplashActivity.this, HomePageActivity.class);
            startActivity(i);
            this.finish();
        }
        else{
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
            this.finish();
        }


    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
