package tinkersolutions.com.dperfumary.Items;

import java.util.ArrayList;

public class ItemNamePrice {

    private ArrayList<Integer> perfumeId;
    private ArrayList<String> perfumeName;
    private ArrayList<Integer> perfumePrice;
    private ArrayList<String> perfumeDescription;
    private ArrayList<String> perfumeURL;
    private ArrayList<Integer> perfumeMinimumOrder;
    private ArrayList<Integer> perfumeAvailableQuantity;

    public ItemNamePrice(ArrayList<Integer> perfumeId, ArrayList<String> perfumeName, ArrayList<Integer> perfumePrice, ArrayList<String> perfumeURL, ArrayList<Integer> perfumeMinimumOrder, ArrayList<Integer> perfumeAvailableQuantity){

        this.perfumeId=perfumeId;
        this.perfumeName=perfumeName;
        this.perfumePrice=perfumePrice;
        this.perfumeURL=perfumeURL;
        this.perfumeMinimumOrder=perfumeMinimumOrder;
        this.perfumeAvailableQuantity=perfumeAvailableQuantity;

    }



    public ArrayList<String> getPerfumeName() {
        return perfumeName;
    }

    public void setPerfumeName(ArrayList<String> perfumeName) {
        this.perfumeName = perfumeName;
    }

    public ArrayList<Integer> getPerfumePrice() {
        return perfumePrice;
    }

    public void setPerfumePrice(ArrayList<Integer> perfumePrice) {
        this.perfumePrice = perfumePrice;
    }

    public ArrayList<String> getPerfumeDescription() {
        return perfumeDescription;
    }

    public void setPerfumeDescription(ArrayList<String> perfumeDescription) {
        this.perfumeDescription = perfumeDescription;
    }

    public ArrayList<Integer> getPerfumeId() {
        return perfumeId;
    }

    public void setPerfumeId(ArrayList<Integer> perfumeId) {
        this.perfumeId = perfumeId;
    }

    public ArrayList<String> getPerfumeURL() {
        return perfumeURL;
    }

    public void setPerfumeURL(ArrayList<String> perfumeURL) {
        this.perfumeURL = perfumeURL;
    }

    public ArrayList<Integer> getPerfumeMinimumOrder() {
        return perfumeMinimumOrder;
    }

    public void setPerfumeMinimumOrder(ArrayList<Integer> perfumeMinimumOrder) {
        this.perfumeMinimumOrder = perfumeMinimumOrder;
    }

    public ArrayList<Integer> getPerfumeAvailableQuantity() {
        return perfumeAvailableQuantity;
    }

    public void setPerfumeAvailableQuantity(ArrayList<Integer> perfumeAvailableQuantity) {
        this.perfumeAvailableQuantity = perfumeAvailableQuantity;
    }
}
