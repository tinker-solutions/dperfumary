package tinkersolutions.com.dperfumary.Items;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import tinkersolutions.com.dperfumary.Options.CartListActivity;
import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.SharedLoginData;
import tinkersolutions.com.dperfumary.SharedPreference.SharedUserData;
import tinkersolutions.com.dperfumary.StartUp.HomePageActivity;



public class ItemDescriptionActivity extends AppCompatActivity {

    int item_price,item_id,minimum_order=20,available_quantity=-999;
    String item_name,item_description,item_image,jsonString;

    TextView tvItemName, tvItemPrice, tvItemDescription, tvAddToCart;
    ImageView imageView,toolbarCart;
    Toolbar toolbar;

    SharedUserData sharedUserData;
    SharedLoginData sharedLoginData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_description);

        initialize();

        tvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Toast.makeText(ItemDescriptionActivity.this, "after"+minimum_order, Toast.LENGTH_SHORT).show();

                if(available_quantity>=minimum_order)
                {
                    sharedUserData.addItem(item_id,minimum_order);
                    Toast.makeText(ItemDescriptionActivity.this, "Item added to cart.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(ItemDescriptionActivity.this, "Item out of stock. Please come after some time", Toast.LENGTH_SHORT).show();
                }


            }
        });

        toolbarCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(ItemDescriptionActivity.this, CartListActivity.class);
                intent.putExtra("maindata",jsonString);
                startActivity(intent);

            }
        });





    }

    @SuppressLint("SetTextI18n")
    private void initialize() {

        Intent intent=getIntent();

        item_id=intent.getIntExtra("perfume_id",-999);
        item_price=intent.getIntExtra("perfume_price",-999);
        item_image=intent.getStringExtra("perfume_picture");
        item_name=intent.getStringExtra("perfume_name");
        item_description=intent.getStringExtra("perfume_description");
        minimum_order=intent.getIntExtra("perfume_minimum_order",-999);
        available_quantity=intent.getIntExtra("perfume_available_quantity",-999);

        jsonString=intent.getStringExtra("maindata");



        tvItemName =findViewById(R.id.item_name);
        tvItemPrice =findViewById(R.id.item_price);
        tvItemDescription =findViewById(R.id.item_description);
        tvAddToCart =findViewById(R.id.addtocart);
        toolbar=findViewById(R.id.toolbar);
        toolbarCart=findViewById(R.id.toolbar_cart);
        imageView=findViewById(R.id.image1);
        sharedUserData=new SharedUserData(this);
        sharedLoginData=new SharedLoginData(this);


        tvItemName.setText(item_name);
        tvItemPrice.setText("Rs. "+String.valueOf(item_price));
        tvItemDescription.setText(item_description);

        Picasso.with(this)
                .load(item_image)
                .placeholder(R.drawable.per) // optional
                .error(R.drawable.per)// optional
                .resize(200, 200) // resizes the image to these dimensions (in pixel)
                .centerCrop()
                .into(imageView);


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,HomePageActivity.class));
    }
}
