package tinkersolutions.com.dperfumary.Options;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import tinkersolutions.com.dperfumary.Adapters.CartAdapter;
import tinkersolutions.com.dperfumary.JsonParser.ItemUtils;
import tinkersolutions.com.dperfumary.JsonParser.Perfumes;
import tinkersolutions.com.dperfumary.R;
import tinkersolutions.com.dperfumary.SharedPreference.ItemIdQuantity;
import tinkersolutions.com.dperfumary.SharedPreference.SharedUserData;
import tinkersolutions.com.dperfumary.StartUp.HomePageActivity;

public class CartListActivity extends AppCompatActivity {


    ItemUtils itemUtils;
    ListView listView;
    Toolbar toolbar;
    List<ItemIdQuantity> itemIdQuantityList;
    SharedUserData sharedUserData;
    Bundle bundle;
    String maindata;
    List<Perfumes> orderList,mainList;
    Gson gson;
    CartAdapter cartAdapter;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);

        initialize();

        setCartAdapter();

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<ItemIdQuantity> finalList=sharedUserData.getCartItems();
                String finalorder=gson.toJson(finalList);
                Toast.makeText(CartListActivity.this, ""+finalorder, Toast.LENGTH_SHORT).show();



            }
        });



    }


    private void initialize() {

        listView=findViewById(R.id.listview);
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=findViewById(R.id.listview);
        textView=findViewById(R.id.final_amount);

        gson=new Gson();
        itemUtils=new ItemUtils();

        itemIdQuantityList=new ArrayList<>();
        orderList=new ArrayList<>();
        mainList=new ArrayList<>();

        bundle=getIntent().getExtras();
        maindata=bundle.getString("maindata");


        itemUtils = gson.fromJson(maindata, ItemUtils.class);
        mainList=itemUtils.getPerfumesList();

        sharedUserData=new SharedUserData(this);
        itemIdQuantityList=sharedUserData.getCartItems();



    }


    private void setCartAdapter() {



        orderList.clear();


        if((itemIdQuantityList==null))
        {
            Toast.makeText(this, "Cart is empty. Shop Now ", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(CartListActivity.this,HomePageActivity.class));
        }
        else if(itemIdQuantityList.size()==0)
        {
            Toast.makeText(this, "Cart is empty. Shop Now ", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(CartListActivity.this,HomePageActivity.class));
        }
else {
            for (int i = 0; i < itemIdQuantityList.size(); i++) {

                for (int j = 0; j < mainList.size(); j++) {
                    if (mainList.get(j).getPerfumeId() == itemIdQuantityList.get(i).getItemId()) {
                        orderList.add(mainList.get(j));
                    }
                }
            }
            cartAdapter=new CartAdapter(CartListActivity.this,orderList,itemIdQuantityList);
            listView.setAdapter(cartAdapter);

            Toast.makeText(this, "" + itemIdQuantityList.size(), Toast.LENGTH_SHORT).show();

        }





    }



    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, HomePageActivity.class));
    }



}
